---
layout: markdown_page
title: "Category Direction - Organization"
description: ""
canonical_path: "/direction/enablement/tenant-scale/organization/"
---

- TOC
{:toc}

## Organization

| | |
| --- | --- |
| Stage | [Data Stores](/direction/enablement/) |
| Maturity | [Not Applicable](/direction/maturity/) |
| Content Last Reviewed | `2023-05-15` |

Thanks for visiting this category direction page on Organizations at GitLab. The Organization category is part of the [Tenant Scale group](https://about.gitlab.com/handbook/product/categories/#tenant-scale-group) within the [Enablement](https://about.gitlab.com/direction/enablement/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute:
* Please comment in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=Category%3AOrganization&label_name%5B%5D=group%3A%3Atenant%20scale&first_page_size=100) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Organization&label_name[]=group::tenant+scale). Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy.
* Please share feedback directly via [email](https://gitlab.com/lohrc), or [schedule a video call](https://calendly.com/christinalohr/30min). If you're a GitLab user and have direct feedback about your needs for the organization, we'd love to hear from you.
* Please open an issue using the ~"Category:Organization" label, or reach out to us internally in the #g_tenant-scale Slack channel.

### Overview

The Organization category focuses on creating a better experience for organizations to manage their GitLab implementation. This includes making administrative capabilities that previously only existed for self-managed users available to our SaaS users as well, such as management of users and groups and supporting cascading settings. The result of this effort will be a more intuitive user experience for all our users towards more aligned behaviors throughout our product.

#### Opportunities

Iterating on Organizations will allow us to solve a number of problems with the current SaaS experience:
* **Increase isolation** - We need mechanisms to isolate an organization from the rest of GitLab.com so that we can meet enterprise regulatory and security requirements. Creating clear organization boundaries in GitLab.com  will prevent users from unintentionally exposing users, projects, or other sensitive information to the rest of the GitLab.com instance. Am organization should feel like a self-contained space where an enterprise can work independently of the rest of Gitlab.com. Users in a group owned by an enterprise should be able to complete all their day to day activities without leaving their organization.
* **Additional control** - On GitLab.com, user accounts and all associated details are owned by a user, not the groups they belong to. This is problematic for group administrators since they can't ensure the level of data integrity necessary for their audit and regulatory needs. We need to provide group administrators additional management capabilities for users in their group.
* **Self-managed feature parity** - In order to satisfy the needs of enterprise customers on GitLab.com, we need to offer an equivalent to the group and project administration experience found in self-managed instances. To meet this goal, we must provide GitLab teams a framework to build settings and features and apply them to an entire group/project hierarchy.

### Vision

The video below contains a discussion with Sid on this topic outlining the problem and the concept of an "instance group". We have since moved away from the term "instance group" and are instead using the term "organization".

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/U8wTrxM35ow" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

The video below gives a walkthrough of our MVC plan for Organization.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BJ9XYpbzuoI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Today, GitLab's features exist at 3 levels:

| Level | Description | Example |
| ------ | ------ | ------ |
| Instance | Features for the entire instance. These are generally admin features (restricted to the admin panel or via a config like `gitlab.rb`), but not always ([Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/)). | [LDAP](https://docs.gitlab.com/ee/administration/auth/ldap/#configuration-core-only), [admin-level push rules](https://docs.gitlab.com/ee/user/admin_area/#admin-area-sections) |
| Group | Features configured and used at the group-level. These generally inherit behavior or objects down into subgroups (like epics, settings, or memberships). | [Epics](https://docs.gitlab.com/ee/user/group/epics/) |
| Project | Features used at the project level. | [Requirements Management](https://docs.gitlab.com/ee/user/project/requirements/) |

This leads to a few problems:
* 3 ways to build a feature lead to follow-on requests to build a feature at some other level (see meta issues like [group level things](https://gitlab.com/gitlab-org/gitlab/-/issues/17836)) and additional engineering effort.
* Restricts the audience, especially with instance-level features. Many of these features are admin-only, which is a tiny percentage of most users on an instance. If we make a feature instance level, we're locking ourselves into a few thousand self-managed users and locking out GitLab.com users.
* Poor UX. Inconsistencies between the features available at the project and group levels create navigation and usability issues. Moreover, there isn't a dedicated place for organization-level features, so cool features like the Operations Dashboard and the Security Dashboard get relegated to the "More" dropdown in the top Navigation Bar.

While it would be ideal to have one way to build a new feature, most GitLab functionality should exist at the group level rather than the instance level at a minimum.

We should extract features from the admin panel into a new object called *Organization* that cascades behavior to all the projects and groups that are owned by the same user or organization. 
* An organization includes settings, data, and features from all groups, subgroups, and projects under the same owner (including personal namespaces).
* Data from all groups, subgroups and projects in an organization can be aggregated. 

### Goals

The Organization group focuses on creating a better experience for organizations to manage their GitLab experience. This includes making administrative capabilities that previously only existed for self-managed users available to our SaaS users as well, such as management of users and groups and supporting cascading settings. This also introduces a new concept for feature parity between projects, subgroups and groups. The result of this effort will be a more intuitive user experience for all our users towards more aligned behaviors throughout our product.

#### Near term goals

* [Migrate basic functionality of groups and projects to namespaces](https://gitlab.com/groups/gitlab-org/-/epics/6585) while working with other teams to migrate their features to namespaces.
* Move administrative features to the [group level](https://gitlab.com/groups/gitlab-org/-/epics/7411) for group owners.

#### Mid term goals

* [Consolidate & Cascade settings](https://gitlab.com/groups/gitlab-org/-/epics/4419) - this will allow cascading and enforcement of settings top down in the hierarchy.
* [Implement the organization](https://gitlab.com/groups/gitlab-org/-/epics/9266) - this will be a new entity that serves as an umbrella for top-level groups belonging to the same organization. We will create a new landing page experience for organizations to easily view and action on different aspects of their groups, projects, users, settings and more.

## Terminology 

Namespace is the logical object container in the code. 
Organization is the name for the entity that hosts top-level groups.

### Demo of introducing the new hierarchy concept for groups and projects for epics.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/fE74lsG_8yM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## What is not planned right now

* The Tenant Scale team will not be responsible for migrating all features from a group or project to a namespace. We are [building a framework](https://gitlab.com/groups/gitlab-org/-/epics/6473) and are [documenting the process](https://docs.gitlab.com/ee/development/organization/index.html#consolidate-groups-and-projects) for features to be migrated. We collaborate with the respective teams to migrate existing functionality to namespace.
* Since we're focused on consolidating groups and projects at this time, we don't plan to make significant improvements to them, as this would create redundant work. Instead, we will have targeted initiatives on specific product areas such as the group and project dashboard to improve the experience holistically between groups and projects.
* We are not planning to make improvements to user profiles in the near term since the Organization group's capacity will be dedicated to merging groups and projects.