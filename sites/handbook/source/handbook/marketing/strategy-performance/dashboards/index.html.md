---
layout: handbook-page-toc
title: "Marketing Dashboards"
description: "The MS&A creates and maintains dashboards using our marketing metrics"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hierarchy of Marketing Dashboards

MS&A uses a hierarchical system when building dashboards to help guide the allowable complexity and frame the data for the intended user.

At the highest level, TD Marketing Key Dashboards dashboards are intended to provide insights into our top KPIs, while TD Marketing Functional Dashboards give the detailed insights functional marketing teams need. Ad hoc dashboards are meant to answer one-off questions, and are not meant to be used for ongoing KPI reporting.

| Level              | Intended User                   | Intended Use (examples)      |
|--------------------|---------------------------------|------------------------------|
| TD Marketing Key Dashboards           | Execs / All GitLab Team Members | Top KPIs and Targets         |
|                    | GTM Teams / All of Marketing    | Tracking Leading Indicators / Attributed Pipeline |
| TD Marketing Functional Dashboards | Functional Marketing Teams      | Campaign Performance / SDR Performance / Partner Sourced Opportunities   |
| Ad-Hoc             | Specific Team Members           | Insight into a one-off question |

## Marketing Dashboards Supported by MS&A

### TD Marketing Key Dashboards

- [Marquee Marketing Metrics Dashboard](https://app.periscopedata.com/app/gitlab:safe-intermediate-dashboard/1070254/FY23-Marketing-Leadership-dashboard) - Tracks high-level KPIs across Awareness, Consideration, Conversion, Expansion & Evangelism.

### TD Marketing Functional Dashboards

- [TD - Campaign Performance](https://app.periscopedata.com/app/gitlab:safe-intermediate-dashboard/1111393/TD---Campaign-Performance) - The source of truth dashboard for marketing campaign performance. It is a single pane of glass view for tracking campaign performance from an Inquiry to a closed won SAO.
- [TD: SDR Performance Dashboard](https://app.periscopedata.com/app/gitlab:safe-intermediate-dashboard/965068/TD:-SDR-Performance-Dashboard---v1.2) - A management-level view of overall SDR performance by a number of key metrics. 
- [TD: Partner Sourced Opportunities](https://app.periscopedata.com/app/gitlab:safe-intermediate-dashboard/983640/TD:-Partner-Sourced-Opportunities) - A view of partner sources opportunities.


## Dashboard Development and Publishing Stages

MS&A uses the stages listed below to denote a dashboard's stage. 
Ad-Hoc dashboards do not have a stage.

- Work in Progress (WIP) - Active development is still ongoing. Metrics, design, and location may change at any time. During this stage, we work with stakeholders to create an end product that meets their needs.

- User Acceptance Testing (UAT) - Primary development has been completed, and we are gathering feedback from the intended users and fixing bugs.

- Production - For a dashboard to be considered production, it must be published in the Production area of Tableau and have gone through the [Tableau approval process](https://about.gitlab.com/handbook/business-technology/data-team/platform/tableau/#governance-model).

## Dashboard Building Guidelines

MS&A has agreed on high-level guidelines when presenting data in any visualization:

- Avoid showing fractional KPIs on key metrics that are not percentages (SAOs, MQLs, INQs). Instead, truncate the number. EG: 43 instead of 43.21
